################################################################################
#             KRY  proj2, Michal Hradecky, xhrade08                            #
################################################################################
import sys
from OracleModule import paddingOracle
# from OracleModule import genNewKey
# from OracleModule import setKey
# from OracleModule import encrypt

'''
Utok budete provadet na funkci paddingOracle():
    
paddingOracle(ciphertext):
  Funkce "zjisti" zda je zasifrovany plaintext korektne zarovnan podle PKCS#7
  a vrati tuto informaci v podobe True/False.
  Parametr ciphertext je retezec zasifrovaneho textu prevedeny do hexa formatu!



Pro jistotu upozorneni:
  Nezapomente, ze zasifrovany text je v rezimu "CBC s nahodnym IV" ve formatu:
      IV | CT

  IV - inicializacni vektor (16 bajtu)
  |  - kontatenace
  CT - zasifrovany text rezimem CBC (nasobek 16 bajtu)



Pro testovani muzete pouzit funkce genNewKey(), setKey(key) a encrypt(plaintext).
---------------------
genNewKey():
  Provede vygenerovani noveho klice, ktery zaroven nastavi jako aktualni sifrovaci
  klic pro padding orakulum. Rovnez vrati vygenerovany klic (ascii, nikoli hexa).

setKey(key):
  Provede nastaveni sifrovaciho klice pro padding orakulum. Argument key ocekava
  sifrovaci klic v ascii, nikoli jako hexa retezec.
  
encrypt(plaintext):
  Provede zarovnani PKCS#7 ascii plaintextu a nasledne jeho zasifrovani 
  s vyuzitim aktualne nastaveneho sifrovaciho klice, ktery sdili s padding 
  orakulem. Sifrovani probiha algoritmem AES-CBC (128b varianta). 
'''

def cutPadding(plaintextWithPadding):
    paddingLength = ord(plaintextWithPadding[-1]) # delka zarovnani

    if (paddingLength > 16):
        return "FAIL posledni znak ma ord. hodnotu vetsi nez 16."

    padding = plaintextWithPadding[-paddingLength:]

    if padding.count(padding[0]) == paddingLength:
        return plaintextWithPadding[:-paddingLength]
    else:
        return "FAIL znak y(=posledniho znak) neni obsazen v plaintextu na poslenich y pozicich"

def decodeCiphertext(ciphertext):
    # zde provedte utok CBC Padding Oracle

    C = ciphertext.decode("hex")
    block_length = 16
    C_blocks = [C[i:i+block_length] for i in range(0, len(C), block_length)]


    plaintext = ""          # vylusteny plaintext
    previous_C_blocks = ""  # vsechny uz zpracovane bloky cisteho ciphertextu - vcetne IV

    # pres vsechny C bloky
    for current_block_index in range(1,len(C_blocks)):
        plaintext_block = ['*'] * block_length  # na zacatku vyplnime hvezdickama - je videt kdyz se neco domrsi
        reseni = {}  # vsechna nalezena reseni pro jednotlive byty v jednom bloku

        # pres vsechny Byty jednoho bloku -> 15,14,13,...,0
        for char_index in reversed(range(0,16)):

            ok = False
            while not ok:
                reseni[char_index]=[]

                # zkousime vsech 256 moznosti 0..255
                for ord_number in range(0,256):
                    C0 = list(C_blocks[current_block_index-1])

                    # xorujeme uz uhodnute
                    for i in (range(char_index+1,16)):
                        C0[i] = chr(ord(C0[i]) ^ ord(plaintext_block[i]) ^ (16-char_index))

                    # xorujeme znak ktery hadame
                    C0[char_index] = chr(ord(C0[char_index]) ^ ord_number ^ (16-char_index))

                    # slozime dohromady request pro orakulum
                    request = previous_C_blocks + "".join(C0) + C_blocks[current_block_index]

                    # pokud orakulum vrati True = uhodli jsme znak
                    if paddingOracle(request.encode("hex")):
                        plaintext_block[char_index] = chr(ord_number)
                        reseni[char_index].append(ord_number)

                # pokud jsem se v predchozim bytu netrefil do spravneho reseni, tak vyberu jine reseni a tento zkusim znovu
                if len(reseni[char_index])>2 or len(reseni[char_index])==0:
                    if len(reseni[char_index+1])>1:
                        del reseni[char_index+1][-1]
                        plaintext_block[char_index+1] = chr(reseni[char_index+1][-1])
                    else:
                        print "Nepovedlo se desifrovat C=" + "".join(C_blocks)
                        sys.exit(1)
                else:
                    ok = True

        plaintext = plaintext + "".join(plaintext_block)
        previous_C_blocks = previous_C_blocks + C_blocks[current_block_index-1]

    plaintextWithoutPadding = cutPadding(plaintext)
    return plaintextWithoutPadding

if __name__ == "__main__":
    if len(sys.argv) > 1:
        ciphertext = sys.argv[1]
    else:
        ciphertext = "fa485ab028cb239a39a9e52df1ebf4c30911b25d73f8906cc45b6bf87f7a693f47609094ccca42050ad609bb3cf979ac"

    # vypis desifrovaneho textu provedte nasledujicim zpusobem: 
    print decodeCiphertext(ciphertext)
    sys.exit(0)